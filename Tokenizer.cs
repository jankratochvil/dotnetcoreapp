using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;

namespace TestApi
{
	public static class TokenizerWrapper
	{
		[DllImport("libbionano_api.so")]
		private static extern IntPtr TokenizerWrapper_Create();
		[DllImport("libbionano_api.so")]
		private static extern void TokenizerWrapper_Delete(IntPtr ptr);
		[DllImport("libbionano_api.so")]
		private static extern bool TokenizerWrapper_ReadFile(IntPtr ptr, string path);
		[DllImport("libbionano_api.so")]
		private static extern int TokenizerWrapper_GetNumOfRecords(IntPtr ptr);
		[DllImport("libbionano_api.so")]
		private static extern bool TokenizerWrapper_HasRecord(IntPtr ptr);
		[DllImport("libbionano_api.so")]
		private static extern bool TokenizerWrapper_GetNextRecord(IntPtr ptr);
		/*
		 * 		String is sent as 'const char*'. Here we need to read it as a pointer
		 * 		and convert to string using Marshal, see below
		 */
	   	[DllImport("libbionano_api.so", CallingConvention = CallingConvention.StdCall, EntryPoint = "TokenizerWrapper_GetChrom", ExactSpelling = false)]
	   	private extern static IntPtr TokenizerWrapper_GetChrom(IntPtr ptr);
	   	private static string TokenizerWrapper_GetChrom_String(IntPtr ptr)
	   	{
		   	return Marshal.PtrToStringAnsi(TokenizerWrapper_GetChrom(ptr));
	   	}
		[DllImport("libbionano_api.so")]
		private static extern int TokenizerWrapper_GetChromStart(IntPtr ptr);
		[DllImport("libbionano_api.so")]
		private static extern int TokenizerWrapper_GetChromEnd(IntPtr ptr);

		private static IntPtr s_Ptr;

		/*
		 * Read contents of '@param file' found in bin folder and populate list '@param records'
		 */
		public static void Populate(string file, ref List<RecordBED> records)
		{
			// folder where executed binary is located
			string binPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
			// strip the 'file:' prefix
			binPath = binPath.Substring(5);
			string path = String.Format("{0}/{1}", binPath, file);

			s_Ptr = TokenizerWrapper_Create();
			bool readFileSuccess = TokenizerWrapper_ReadFile(s_Ptr, path);
			// bool hasRecord = TokenizerWrapper_HasRecord(ptr);
			int num = TokenizerWrapper_GetNumOfRecords(s_Ptr);

			Console.WriteLine(String.Format("Reading file '{0}' {1}", path, readFileSuccess ? "success" : "failed"));
			if (!readFileSuccess) return;
			Console.WriteLine(String.Format("Found {0} records", num));

			while (TokenizerWrapper_GetNextRecord(s_Ptr))
			{
				RecordBED record;
				record.chrom = TokenizerWrapper_GetChrom_String(s_Ptr);
				record.chromStart = TokenizerWrapper_GetChromStart(s_Ptr);
				record.chromEnd = TokenizerWrapper_GetChromEnd(s_Ptr);

				records.Add(record);

				Console.WriteLine(record);
			}
			TokenizerWrapper_Delete(s_Ptr);
		}
	}
}

