﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace TestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
		private static List<RecordBED> s_records;
		private static string[] s_stringRecords;

		static ValuesController()
		{
			s_records = new List<RecordBED>();
			TokenizerWrapper.Populate("test.bed", ref s_records);
			s_stringRecords = new string[s_records.Count()];
			int i = 0;
			foreach(var x in s_records)
			{
				s_stringRecords[i] = x.ToStringSeparated();
				++i;
			}
		}

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
			return s_stringRecords;
            // return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
