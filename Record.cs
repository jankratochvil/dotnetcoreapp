using System;

namespace TestApi
{
	public struct RecordBED
	{
		public string 	chrom;
		public int 		chromStart;
		public int 		chromEnd;

		public override string ToString ()
		{
			return String.Format("{0}\t{1}\t{2}", chrom, chromStart, chromEnd);
		}
		public string ToStringSeparated()
		{
			return String.Format("{0},{1},{2}", chrom, chromStart, chromEnd);
		}
	}
}
